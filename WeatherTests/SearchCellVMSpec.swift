//
//  SearchCellVMSepc.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 28..
//  Copyright © 2017년 weather. All rights reserved.
//

@testable import Weather
import Quick
import Nimble
import Result
import ReactiveSwift


class SearchCellVMSpec : QuickSpec {
    override func spec() {
        
        
        describe("SearchCellViewModel Tests") {
            var viewmodels : Array<SearchCellVM>?
            var datasource : Array<UILabel>?
            beforeEach {
                
                viewmodels = [SearchCellVM(with: "1"), SearchCellVM(with: "2"), SearchCellVM(with: "3")]
                datasource = [UILabel(), UILabel(), UILabel()]
            }
            it( "SearchCellViewModel data bind test" ) {
                for i in 0 ..<  datasource!.count {
                    let label = datasource![i]
                    let vm = viewmodels![i]
                    label.rac_text <~ vm.rac_query
                }
                
                
                expect( datasource![2].text ).toEventually(equal("3"), timeout: 1.0, pollInterval: 0.0, description: "Binded label text should 3 but [\(datasource![2].text)]")
            }
            
            it( "SearchCellViewModel data bind and change test" ) {
                for i in 0 ..<  datasource!.count {
                    let label = datasource![i]
                    let vm = viewmodels![i]
                    label.rac_text <~ vm.rac_query
                }
                
                viewmodels![2].rac_query.value = "4"
                expect( datasource![2].text ).toEventually(equal("4"), timeout: 1.0, pollInterval: 0.0, description: "Binded label text should 4 but [\(datasource![2].text)]")
            }
        }
    }
}
