//
//  WeatherResultCellVMSpec.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

@testable import Weather
import Quick
import Nimble
import Result
import ReactiveSwift


class WeatherResultCellVMSpec: QuickSpec {
    
    override func spec() {
        
        var viewmodel : WeatherResultCellsVM?
        describe("Weather Result Cell View Model Tests") {
            
            
            it( "Data load verification" ) {
                let image = MutableProperty<UIImage?>( nil )
                let observationtime = MutableProperty<String?>(nil)
                let humidity = MutableProperty<String?>(nil)
                let description = MutableProperty<String?>(nil)
                var datamodel = WEATHER( with : NSDictionary() )
                datamodel.humidity = "10.0"
                datamodel.weatherDesc = "Partly Cloudy"
                datamodel.observation_time = "14 : 00"
                datamodel.weatherIcon = IMAGE(with: NSDictionary() )
                datamodel.weatherIcon?.url = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0008_clear_sky_night.png".url?.https
                
                viewmodel = WeatherResultCellsVM(with: datamodel)
                image <~ viewmodel!.rac_weatherIcon
                observationtime <~ viewmodel!.rac_observationTime
                humidity <~ viewmodel!.rac_humidity
                description <~ viewmodel!.rac_weatherDescription
                
                expect( observationtime.value ).toEventually(equal( datamodel.observation_time ), timeout: 1.0, pollInterval: 0.0, description: "observation time value should be equal to datamodel")
                expect( humidity.value ).toEventually(equal( datamodel.humidity ), timeout: 1.0, pollInterval: 0.0, description: "humidity value should be equal to datamodel")
                expect( description.value ).toEventually(equal( datamodel.weatherDesc ), timeout: 1.0, pollInterval: 0.0, description: "description value should be equal to datamodel")
                expect( image.value ).toEventuallyNot(beNil(), timeout: 2.0, pollInterval: 0.0, description: "weather Icon value could be downloaded")
            }

        }
    }
    
}
