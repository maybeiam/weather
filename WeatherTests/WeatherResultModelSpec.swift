//
//  SearchResultModelSpec.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 28..
//  Copyright © 2017년 weather. All rights reserved.
//

@testable import Weather
import Quick
import Nimble
import Result
import ReactiveSwift

class WeatherResultModelSpec: QuickSpec {
    
    override func spec() {
        
        var model : WeatherResultModel?
        describe("Weather Result Model Tests") {
            
            beforeEach {
                let request = REQUEST(with: ["type":"City", "query":"London"] as NSDictionary )
                model = WeatherResultModel(request: request, weather: WEATHER( with : NSDictionary() ), network: MockNetwork_Successful())
            }
            it( "reload data test" ) {
                let weather = MutableProperty<WEATHER?>(nil)
                let loading = MutableProperty<Bool>(false)
                weather <~ model!.outputs.rac_weather
                loading <~ model!.outputs.rac_loading
                expect( loading.value ).to(equal( false ), description : "start value of rac_loading should be false" )
                model?.inputs.reloadDataFromServer()
                
                expect( weather ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "weather value after reload request should not be nil but [\(weather)]")
            }
        }
    }
    
}
