//
//  CoreDataPersistentStorageSpec.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 27..
//  Copyright © 2017년 weather. All rights reserved.
//

@testable import Weather
import CoreData
import Quick
import Nimble
import ReactiveSwift
import Result

class CoreDataPersistentStorageSpec : QuickSpec {
    let coredata = CoreDataPersistentStorage.sharedInMemory
    
    override func spec() {
        context( "CoreDataStorage Tests") {
            
            it( "Insert data" ) {
                var array : Array<NSObject>?
                var fetchError : StorageFetchError?
                self.coredata.writeSuccesfulQuery(query: "1")
                self.coredata.fetchSuccesfulQuery().startWithSignal({ (signal, disposable) in
                    signal.observeResult({ (result) in
                        array = result.value
                        fetchError = result.error
                    })
                })
                expect( array ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Result of fetching successful query should not nil but [\(array)]")
                expect( fetchError ).toEventually(beNil(), timeout: 2.0, pollInterval: 0.0, description: "First result of fetching successful query error should be nil but [\(fetchError)]")
                expect( array!.count ).toEventually(equal(1), timeout: 2.0, pollInterval: 0.0, description: "Result of fetching successful query count should be 1 but [\(array!.count)]")
            }
            
            it( "Duplicate test" ) {
                var array : Array<NSObject>?
                var fetchError : StorageFetchError?
                self.coredata.writeSuccesfulQuery(query: "1")
                self.coredata.writeSuccesfulQuery(query: "1")
                self.coredata.writeSuccesfulQuery(query: "1")
                self.coredata.writeSuccesfulQuery(query: "1")
                self.coredata.writeSuccesfulQuery(query: "1")
                self.coredata.writeSuccesfulQuery(query: "1")
                self.coredata.fetchSuccesfulQuery().startWithSignal({ (signal, disposable) in
                    signal.observeResult({ (result) in
                        array = result.value
                        fetchError = result.error
                    })
                })
                expect( array ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Result of fetching successful query should not nil but [\(array)]")
                expect( fetchError ).toEventually(beNil(), timeout: 2.0, pollInterval: 0.0, description: "First result of fetching successful query error should be nil but [\(fetchError)]")
                expect( array!.count ).toEventually(equal(1), timeout: 2.0, pollInterval: 0.0, description: "Result of fetching successful query count should be 1 but [\(array!.count)]")
            }
            
            
            it( "Insert 10 datas" ) {
                var array : Array<NSObject>?
                var fetchError : StorageFetchError?
                self.coredata.writeSuccesfulQuery(query: "1")
                self.coredata.writeSuccesfulQuery(query: "2")
                self.coredata.writeSuccesfulQuery(query: "3")
                self.coredata.writeSuccesfulQuery(query: "4")
                self.coredata.writeSuccesfulQuery(query: "5")
                self.coredata.writeSuccesfulQuery(query: "6")
                self.coredata.writeSuccesfulQuery(query: "7")
                self.coredata.writeSuccesfulQuery(query: "8")
                self.coredata.writeSuccesfulQuery(query: "9")
                self.coredata.writeSuccesfulQuery(query: "10")
                self.coredata.fetchSuccesfulQuery().startWithSignal({ (signal, disposable) in
                    signal.observeResult({ (result) in
                        array = result.value
                        fetchError = result.error
                    })
                })
                expect( array ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Result of fetching successful query should not nil but [\(array)]")
                expect( fetchError ).toEventually(beNil(), timeout: 2.0, pollInterval: 0.0, description: "First result of fetching successful query error should be nil but [\(fetchError)]")
                expect( array!.count ).toEventually(equal(10), timeout: 2.0, pollInterval: 0.0, description: "Result of fetching successful query count should be 10 but [\(array!.count)]")
            }
            
            it( "Insert 11 or more datas" ) {
                var array : Array<NSObject>?
                var fetchError : StorageFetchError?
                self.coredata.writeSuccesfulQuery(query: "1")
                self.coredata.writeSuccesfulQuery(query: "2")
                self.coredata.writeSuccesfulQuery(query: "3")
                self.coredata.writeSuccesfulQuery(query: "4")
                self.coredata.writeSuccesfulQuery(query: "5")
                self.coredata.writeSuccesfulQuery(query: "6")
                self.coredata.writeSuccesfulQuery(query: "7")
                self.coredata.writeSuccesfulQuery(query: "8")
                self.coredata.writeSuccesfulQuery(query: "9")
                self.coredata.writeSuccesfulQuery(query: "10")
                self.coredata.writeSuccesfulQuery(query: "11")
                self.coredata.writeSuccesfulQuery(query: "12")
                self.coredata.writeSuccesfulQuery(query: "13")
                self.coredata.fetchSuccesfulQuery().startWithSignal({ (signal, disposable) in
                    signal.observeResult({ (result) in
                        array = result.value
                        fetchError = result.error
                    })
                })
                expect( array ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Result of fetching successful query should not nil but [\(array)]")
                expect( fetchError ).toEventually(beNil(), timeout: 2.0, pollInterval: 0.0, description: "First result of fetching successful query error should be nil but [\(fetchError)]")
                expect( array!.count ).toEventually(equal(10), timeout: 2.0, pollInterval: 0.0, description: "Result of fetching successful query count should be 10 but [\(array!.count)]")
                
            }
            
            
            
            it( "Last data test" ) {
                var array : Array<NSObject>?
                self.coredata.fetchSuccesfulQuery().startWithSignal({ (signal, disposable) in
                    signal.observeResult({ (result) in
                        array = result.value
                    })
                })
                
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    expect( array!.last!.value(forKey: "query") as? String ).to(equal("4"), description: "Last result of fetching successful query count should be \"4\" but [\(array!.last!.value(forKey: "query") as! String)]")
                    expect( array!.first!.value(forKey: "query") as? String ).to(equal("13"), description: "Result of fetching successful query count should be \"13\" but [\(array!.first!.value(forKey: "query") as! String)]")
                    done()
                })

            }
            
        }
        
        
    }
    
}
