//
//  WEATHERSpec.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 27..
//  Copyright © 2017년 weather. All rights reserved.
//

import XCTest

@testable import Weather
import Quick
import Nimble

class WEATHERSpec : QuickSpec {
    override func spec() {
        
        let stub = "{\"data\":{\"request\":[{\"type\":\"City\",\"query\":\"London\"}],\"current_condition\":[{\"observation_time\":\"04:32 AM\",\"temp_C\":\"-1\",\"temp_F\":\"30\",\"weatherCode\":\"143\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0006_mist.png\"}],\"weatherDesc\":[{\"value\":\"Mist\"}],\"windspeedMiles\":\"11\",\"windspeedKmph\":\"17\",\"winddirDegree\":\"90\",\"winddir16Point\":\"E\",\"precipMM\":\"0.0\",\"humidity\":\"93\",\"visibility\":\"3\",\"pressure\":\"1012\",\"cloudcover\":\"0\"}],\"weather\":[{\"date\":\"2017-01-27\",\"tempMaxC\":\"7\",\"tempMaxF\":\"45\",\"tempMinC\":\"-1\",\"tempMinF\":\"31\",\"windspeedMiles\":\"11\",\"windspeedKmph\":\"19\",\"winddirection\":\"SSE\",\"winddir16Point\":\"SSE\",\"winddirDegree\":\"162\",\"weatherCode\":\"116\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png\"}],\"weatherDesc\":[{\"value\":\"Partly cloudy\"}],\"precipMM\":\"0.1\"}]}}".data(using: .utf8)
        
        
        describe("WEATHER parsing test") {
            var jsonstub : NSDictionary?
            
            beforeEach {
                jsonstub = try! JSONSerialization.jsonObject(with: stub!) as! NSDictionary
            }
            
            it( "Parse from stub to weather array result" ) {
                
                let array = WEATHER.array(from: jsonstub!)
                expect( array ).toNot(beNil(), description: "Parse weather data array from dictionary : result should be not nil, but it is [\(array)]")
                expect( array.count ).to(equal(1), description: "Parse weather data array  from dictionary count : result should be 1, but it is [\(array.count)]")
                
                let weather = array[0]
                expect( weather.observation_time! ).to(equal("04:32 AM"), description: "Parsed weather data variable \"observation_time\" should be 04:32 AM it is [\(weather.observation_time)]")
            }
        }
    }
}
