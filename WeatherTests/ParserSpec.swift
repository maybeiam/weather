//
//  ParserSpec.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 25..
//  Copyright © 2017년 weather. All rights reserved.
//

@testable import Weather
import Quick
import Nimble

class ParserSpec: QuickSpec {
    let errorparser = WeatherErrorParser()
    
    override func spec() {
        context( "Parsers Tests") {
            describe("Error Parser tests") {
                
                it( "WeatherParser with empty dictionary test" ) {
                    let stub : NSDictionary? = nil
                    let result = self.errorparser.parse(with: stub)
                    let msg = result?.object(forKey: "msg") as? String
                    expect( msg ).to(equal("Empty request result error"), description: "Error parser with empty dictionary should be Empty request result error but it is [\(msg)]")
                }
                
                it( "WeatherParser with no data test" ) {
                    let stub = ["nodata":"nodata"] as NSDictionary
                    let result = self.errorparser.parse(with: stub)
                    let msg = result?.object(forKey: "msg") as? String
                    expect( msg ).to(equal("Wrong format error"), description: "Error parser with empty dictionary should be Wrong format error but it is [\(msg)]")
                }
                
                it( "WeatherParser with proper dataset test" ) {
                    let stub = ["data":["error" : [["msg":"Unable to find any matching weather location to the query submitted!"] as NSDictionary] as NSArray] as NSDictionary] as NSDictionary
                    let result = self.errorparser.parse(with: stub)
                    let msg = result?.object(forKey: "msg") as? String
                    let originMessage = (((stub.object(forKey: "data") as! NSDictionary).object(forKey: "error") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "msg") as! String
                    expect( msg ).to(equal(originMessage), description: "Error parser with proper data set dictionary should be [\(originMessage)] but it is [\(msg)]")
                }
            }
        }
        
        
    }
    
}
