//
//  WeatherResultDataSourceSpec.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

@testable import Weather
import Quick
import Nimble
import Result
import ReactiveSwift

class MockWeatherResultCellVM : WeatherResultCellVMOutputs,  WeatherResultCellVMInOuts{
    var outputs : WeatherResultCellVMOutputs { return self }
    
    internal var rac_weatherIcon: MutableProperty<UIImage?> = MutableProperty( nil )
    internal var rac_humidity: MutableProperty<String?> = MutableProperty( nil )
    internal var rac_observationTime: MutableProperty<String?> = MutableProperty( nil )
    internal var rac_weatherDescription: MutableProperty<String?> = MutableProperty( nil )
}

class WeatherResultDataSourceSpec : QuickSpec {
    override func spec() {
        
        var viewmodel : MockWeatherResultCellVM?
        var datasource : WeatherResultDataSource?
        let tableview = UITableView()
        tableview.register(HumidityCell.self, forCellReuseIdentifier: HumidityCell.reuseIdentifier)
        tableview.register(IconCell.self, forCellReuseIdentifier: IconCell.reuseIdentifier)
        tableview.register(DescriptionCell.self, forCellReuseIdentifier: DescriptionCell.reuseIdentifier)
        tableview.register(ObservationTimeCell.self, forCellReuseIdentifier: ObservationTimeCell.reuseIdentifier)
        
        describe("WeatherResultDataSource Tests") {
            
            beforeEach {
                viewmodel = MockWeatherResultCellVM()
                datasource = WeatherResultDataSource()
            }
            
            it( "WeatherResultDataSource data load test" ) {
                datasource?.load(value: [viewmodel!], at: 0)
                let sectionCount = datasource?.numberOfSections(in: tableview)
                let rowCount = datasource?.tableView(tableview, numberOfRowsInSection: 0)
                expect( sectionCount ).to( equal( 4 ), description : "Section count should be 1" );
                expect( rowCount ).to( equal( 1 ), description : "Row count should be 1" );
            }
        }
    }
}
