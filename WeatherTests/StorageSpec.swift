//
//  StorageSpec.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 26..
//  Copyright © 2017년 weather. All rights reserved.
//

@testable import Weather
import CoreData
import Quick
import Nimble
import ReactiveSwift
import Result

private let _sharedMockCoreDataStorage = MockCoreDataStorage()

public enum MockStorageDataSource : StorageRepresentationType {
    case memorycoredata
    
    public var datasource : Fetchable {
        switch self {
        case .memorycoredata:
            return MockCoreDataStorage.shared
        }
    }
}

class MockSuccessfulQuery : NSObject {
    var query : String?
    var date : NSDate?
}

class MockCoreDataStorage : Fetchable {
    static let shared = _sharedMockCoreDataStorage
    internal var fetched = Array<NSObject>()
    
    public func fetchSuccesfulQuery() -> SignalProducer<Array<NSObject>, StorageFetchError> {
        return SignalProducer { observer, disposable in
            observer.send(value: self.fetched)
            observer.sendCompleted()
        }
    }
    
    public func writeSuccesfulQuery(query: String) {
        let querycount = fetched.filter { (managedObject) -> Bool in
            return managedObject.value(forKey: "query" ) as? String == query as String
            }.count
        if querycount > 0 {
            return
        }
        if fetched.count > 10 {
            let first = fetched[0]
            first.setValue(query, forKey: "query")
            first.setValue(NSDate(), forKey: "date")
        } else {
            let object = MockSuccessfulQuery()
            object.date = NSDate()
            object.query = query
            fetched.append( object)
        }
    }
}


class StorageSpec : QuickSpec {
    let storage = Storage(with: MockStorageDataSource.memorycoredata )
    
    override func spec() {
        context( "Storage Tests") {
            describe("CoreData Tests") {
                
                it( "Insert data" ) {
                    var array : Array<SingleQuery>?
                    var fetchError : StorageFetchError?
                    self.storage.insertSuccessfulQuery(query: "1")
                    self.storage.fetchSuccessfulQuery().startWithSignal({ (signal, disposable) in
                        signal.observeResult({ (result) in
                            array = result.value
                            fetchError = result.error
                        })
                    })
                    expect( array ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Result of fetching successful query should not nil but [\(array)]")
                    expect( fetchError ).toEventually(beNil(), timeout: 2.0, pollInterval: 0.0, description: "First result of fetching successful query error should be nil but [\(fetchError)]")
                    
                    waitUntil(timeout: 3.0, action: { (done) in
                        sleep(2)
                        expect( array!.last!.query ).to(equal("1"), description: "Result of fetching successful query count should be \"1\" but [\(array!.last!.query)]")
                        done()
                    })
                }
                
            }
        }
    }
    
}
