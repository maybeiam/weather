//
//  SearchVMSpec.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 28..
//  Copyright © 2017년 weather. All rights reserved.
//

@testable import Weather
import Quick
import Nimble
import Result
import ReactiveSwift

class MockSearchModel : SearchModelInOuts, SearchModelOutputs, SearchModelInputs {
    
    var inputs: SearchModelInputs { return self }
    var outputs : SearchModelOutputs { return self }
    
    init() {
        rac_searchResults = MutableProperty( requests.map({ (string) -> SingleQuery in
            return SingleQuery(with: ["query" : string] as NSDictionary )
        }) )
        let noresultpipe = Signal<String, NoError>.pipe()
        noResultSignal = noresultpipe.0
        noResultObserver = noresultpipe.1
        
        let weatherpipe = Signal<Array<WEATHER>, NoError>.pipe()
        weatherSearchResultSignal = weatherpipe.0
        weatherSearchResultObserver = weatherpipe.1
    }
    
    var requests = Array<String>() {
        didSet {
            rac_searchResults.value = requests.map({ (string) -> SingleQuery in
                return SingleQuery(with: ["query" : string] as NSDictionary )
            })
        }
    }
    
    var noResultSignal: Signal<String, NoError>
    var noResultObserver : Observer< String, NoError >
    var weatherSearchResultSignal: Signal<Array<WEATHER>, NoError>
    var weatherSearchResultObserver : Observer< Array<WEATHER>, NoError >
    func searchRequestReceived(query: String) {
        if query == "wrong" {
            noResultObserver.send(value: "wrong request" )
            return
        }
        requests.append(query)
        weatherSearchResultObserver.send(value: requests.map{ string in return WEATHER(with : NSDictionary()) } )
    }
    var rac_searchResults: MutableProperty<Array<SingleQuery>>
    func fetchSuccessfulQuery() {
        
    }
    var requestWeatherDataPair: (request: REQUEST, weather: WEATHER) {
        return ( request : REQUEST(with: NSDictionary() ), weather : WEATHER( with : NSDictionary()) )
    }
}

class SearchVMSpec : QuickSpec {
    override func spec() {
        
        
        describe("SearchViewModel Tests") {
            var viewmodel : SearchVM?
            var model : MockSearchModel?
            
            beforeEach {
                model = MockSearchModel()
                viewmodel = SearchVM(with: model!)
            }
            it( "SearchViewModel search request test" ) {
                let suggestionlist = MutableProperty( Array<String>() )
                suggestionlist <~ viewmodel!.outputs.rac_suggestionList
                viewmodel!.inputs.searchButtonPressed(query: "London")
                
                expect( suggestionlist.value.count ).toEventually(equal(1), timeout: 1.0, pollInterval: 0.0, description: "Search request test with london and returned suggestion list count should be 1 but [\(suggestionlist.value.count)]")
                
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    let array = suggestionlist.value
                    expect( array.last! ).to(equal("London"), description: "Result of first query count should be \"London\" but [\(array.last!)]")
                    done()
                })
            }
            
            it( "SearchViewModel stored query selection test" ) {
                let suggestionlist = MutableProperty( Array<String>() )
                suggestionlist <~ viewmodel!.outputs.rac_suggestionList
                viewmodel!.inputs.searchButtonPressed(query: "London")
                viewmodel!.inputs.searchButtonPressed(query: "LA")
                
                expect( suggestionlist.value.count ).toEventually(equal(2), timeout: 1.0, pollInterval: 0.0, description: "Search request test with london and returned suggestion list count should be 2 but [\(suggestionlist.value.count)]")
                viewmodel!.inputs.didSelectSuggestion(at: 1)
                
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    let array = suggestionlist.value
                    expect( array.count ).to(equal(3), description: "Result of successful query count should be 3 butbut [\(array.count)]")
                    done()
                })
            }
            
            it( "SearchViewModel tableview hidden property test" ) {
                let isHidden = MutableProperty( true )
                isHidden <~ viewmodel!.outputs.rac_SuggestionListTableViewIsHidden
                viewmodel!.inputs.didEnterSearchTextField()
                
                expect( isHidden.value ).toEventually(equal(false), timeout: 1.0, pollInterval: 0.0, description: "isHidden value should be false but [\(isHidden.value)]")
                
                viewmodel!.inputs.didLeaveSearchTextField()
//
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    expect( isHidden.value ).to(equal(true), description: "isHidden value should be true but [\(isHidden.value)]")
                    done()
                })
            }
            
            it( "SearchViewModel search request response test" ) {
                var vm : WeatherResultVMInOuts?
                viewmodel!.outputs.searchRequestReceivedDataSignal.observeValues { value in
                    vm = value
                }
                var msg = ""
                viewmodel!.outputs.searchRequestReceivedNoDataSignal.observeValues { value in
                    msg = value
                }
                viewmodel!.inputs.didEnterSearchTextField()
                viewmodel!.inputs.searchButtonPressed(query: "London")
                viewmodel!.inputs.searchButtonPressed(query: "LA")
                viewmodel!.inputs.searchButtonPressed(query: "wrong")
                expect( vm ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "response with two request should not be nil")
                expect( msg ).toEventually(equal("wrong request"), timeout: 1.0, pollInterval: 0.0, description: "response with false request should be \"wrong request\" but [\(msg)]")
            }
        }
    }
}
