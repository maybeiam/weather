//
//  SearchModelSpec.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 28..
//  Copyright © 2017년 weather. All rights reserved.
//
@testable import Weather
import Quick
import Nimble
import Result
import ReactiveSwift

public struct MockNetwork_Successful : NetworkType {
    
    public func request(api: APISType) -> SignalProducer<NSDictionary, NetworkError> {
        return SignalProducer { observer, disposable in
            let request = [["type":"City", "query":"London"] as NSDictionary] as NSArray
            let stub = ["data":["request" : request, "current_condition":[NSDictionary()] as NSArray] as NSDictionary] as NSDictionary
            observer.send(value: stub)
        }
    }
}

public struct MockNetwork_NoData : NetworkType {
    public func request(api: APISType) -> SignalProducer<NSDictionary, NetworkError> {
        
        return SignalProducer { observer, disposable in
            let stub = ["msg":"no data", "code" : "101"] as NSDictionary
            observer.send(value: stub)
        }
    }
}

final public class MockStorage : StorageType {
    public init(with storage: StorageRepresentationType ) {
        persistantStorage = storage.datasource
    }
    
    public var persistantStorage : Fetchable
    
    public func fetch<T:DataModelType>(query: Query) -> SignalProducer<Array<T>, StorageFetchError> {
        switch query {
        case .search:
            return SignalProducer { [unowned self] observer, disposable in
                self.persistantStorage.fetchSuccesfulQuery().map{ datas in datas.map{ data in T( with : data ) } }.startWithSignal({ (signal, disposable) in
                    signal.observeResult({ (result) in
                        if result.error == nil {
                            observer.send(value: result.value ?? Array<T>())
                        } else {
                            observer.send(error: result.error!)
                        }
                    })
                })
            }
        }
    }
    
    public func fetchSuccessfulQuery() -> SignalProducer<Array<SingleQuery>, StorageFetchError> {
        return fetch(query: .search)
    }
    
    public func insertSuccessfulQuery(query: String) {
        self.persistantStorage.writeSuccesfulQuery(query: query)
    }
}

class SearchModelSpec : QuickSpec {
    override func spec() {
        
        var successmodel : SearchModel?
        var nodatamodel : SearchModel?
        describe("Search Model Tests") {
            
            beforeEach {
                successmodel = SearchModel(storage: MockStorage(with: MockStorageDataSource.memorycoredata), network: MockNetwork_Successful())
                nodatamodel = SearchModel(storage: MockStorage(with: MockStorageDataSource.memorycoredata), network: MockNetwork_NoData())
            }
            it( "Search request in search model with proper data test" ) {
                var result : Array<WEATHER>? = nil
                let queries = MutableProperty( Array<SingleQuery>() )
                successmodel?.outputs.weatherSearchResultSignal.observeValues{ value in
                    result = value
                }
                queries <~ successmodel!.outputs.rac_searchResults
                successmodel?.inputs.searchRequestReceived(query: "London")
                
                expect( result ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Search request in search model result should not be nil but [\(result)]")
                
                expect( successmodel?.outputs.requestWeatherDataPair.request.query ).toEventually(equal( "London"), timeout: 1.0, pollInterval: 0.0, description: "Last successful request query should be London but [\(successmodel?.outputs.requestWeatherDataPair.request.query)]")
                
                expect( queries.value.count ).toEventually(equal(1), timeout: 1.0, pollInterval: 0.0, description: "Saved successful query count should return 1 but [\(queries.value.count)]")
            }
            
            it( "Search request in search model with nodata test" ) {
                var nodataMessage : String?
                let queries = MutableProperty( Array<SingleQuery>() )
                nodatamodel?.outputs.noResultSignal.observeValues({ (value) in
                    nodataMessage = value
                })
                queries <~ nodatamodel!.outputs.rac_searchResults
                nodatamodel?.inputs.searchRequestReceived(query: "London")
                
                expect( nodataMessage ).toEventually(equal("no data"), timeout: 1.0, pollInterval: 0.0, description: "Search request in search model should not be nil but [\(nodataMessage)]")
                expect( queries.value.count ).toEventually(equal(0), timeout: 1.0, pollInterval: 0.0, description: "Saved successful query count should return 0 but [\(queries.value.count)]")
            }
        }
    }
}
