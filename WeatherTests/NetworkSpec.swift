//
//  NetworkTest.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 24..
//  Copyright © 2017년 weather. All rights reserved.
//

@testable import Weather
import Quick
import Nimble
import Result
import ReactiveSwift

enum MockAPIs : APISType {
    case londonsearch
    case emptydata
    case wrongurl
    case nilrequest
    
    internal var request: URLRequest? {
        switch self {
        case .londonsearch:
            guard let url = (TargetURL.Weather.string + "&q=" + "London").url else {
                return nil
            }
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            return request
            
        case .emptydata:
            guard let url = (TargetURL.Weather.string + "&q=" + "L").url else {
                return nil
            }
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            return request
            
        case .wrongurl:
            guard let url = ("").url else {
                return nil
            }
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            return request
            
        case .nilrequest:
            return nil
        }
    }
}

class NetworkSpec : QuickSpec {
    override func spec() {
        let network = Network()
        describe("Network test") {
            
            it( "Single result network request success case with query : London" ) {
                var dictionary : NSDictionary?
                var networkError : NetworkError?
                network.request(api: MockAPIs.londonsearch).startWithSignal({ (signal, disposable) in
                    signal.observeResult{ result in
                        dictionary = result.value
                        networkError = result.error
                    }
                    
                    signal.observeFailed({ (error) in
                        networkError = error
                    })
                })
                
                expect( dictionary ).toEventuallyNot(beNil(), timeout: 4.0, pollInterval: 0.0, description: "Single result network request success case : result should be not nil, but it is [\(dictionary)]")
                expect( networkError ).toEventually(beNil(), timeout: 4.0, pollInterval: 0.0, description: "Single result network request success case : networkError should be not nil, but it is [\(networkError)]")
            }
            
            it( "Single result network request failure case : Wrong URL" ) {
                var error : NetworkError?
                network.request(api: MockAPIs.wrongurl).startWithSignal({ (signal, disposable) in
                    signal.observeResult{ result in
                        error = result.error
                    }
                })
                
                expect( error ).toEventually(equal(NetworkError.wrongurl), timeout: 2.0, pollInterval: 0.0, description: "Single result with wrong url network request failure case : error should be wrongurl error, but it is [\(error)]")
            }
            
            it( "Single result network request failure case : nil request" ) {
                var error : NetworkError?
                network.request(api: MockAPIs.nilrequest).startWithSignal({ (signal, disposable) in
                    signal.observeResult{ result in
                        error = result.error
                    }
                })
                
                expect( error ).toEventually(equal(NetworkError.wrongurl), timeout: 2.0, pollInterval: 0.0, description: "Single result with nil request failure case : error should be wrongurl error, but it is [\(error)]")
            }
            
            it( "Single result network request failure case : empty data" ) {
//                var error : NetworkError?
                var dictionary : NSDictionary?
                network.request(api: MockAPIs.emptydata).startWithSignal({ (signal, disposable) in
                    signal.observeResult{ result in
                        dictionary = result.value
                    }
                })
                
//                expect( dictionary?.object(forKey: "msg") ).toEventually(equal(NetworkError.nodata), timeout: 2.0, pollInterval: 0.0, description: "Single result with wrong query : error should be nodata error, but it is [\(error)]")
                expect( dictionary?.object(forKey: "msg") as? String ).toEventually(equal("Unable to find any matching weather location to the query submitted!"), timeout: 2.0, pollInterval: 0.0, description: "Single result with wrong query : error should be nodata error, but it is [\(dictionary?.object(forKey: "msg") as? String)]")
            }
        }
    }
}
