//
//  IMAGESpec.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 27..
//  Copyright © 2017년 weather. All rights reserved.
//

import XCTest

@testable import Weather
import Quick
import Nimble

class IMAGESpec : QuickSpec {
    override func spec() {
        
        let stub = "{\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0006_mist.png\"}]}".data(using: .utf8)
        
        
        describe("IMAGE parsing test") {
            var jsonstub : NSDictionary?
            
            beforeEach {
                jsonstub = try! JSONSerialization.jsonObject(with: stub!) as! NSDictionary
            }
            
            it( "Parse from stub to image result" ) {
                let array = jsonstub?.object(forKey: "weatherIconUrl" ) as! NSArray
                let value = array.object(at: 0) as! NSDictionary
                let image = IMAGE(with: value)
                expect( image.url ).toNot(beNil(), description: "Parse IMAGE result should be not nil, but it is [\(image.url)]")
                expect( image.url?.absoluteString ).to(equal("http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0006_mist.png"), description: "Parse IMAGE url from stub should be not nil, but it is [\(image.url?.absoluteString)]")
            }
        }
    }
}
