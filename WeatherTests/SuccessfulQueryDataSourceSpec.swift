//
//  SuccessfulQueryDataSourceSpec.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

@testable import Weather
import Quick
import Nimble
import Result
import ReactiveSwift

class MockSearchCellVM : SearchCellVMOutputs, SearchCellVMInouts {
    var outputs: SearchCellVMOutputs { return self }
    var rac_query: MutableProperty<String> = MutableProperty( "" )
}
class SuccessfulQueryDataSourceSpec : QuickSpec {
    override func spec() {
        
        var viewmodel : MockSearchCellVM?
        var datasource : SuccessfulQueryDataSource?
        let tableview = UITableView()
        tableview.register(SearchTableViewCell.self, forCellReuseIdentifier: SearchTableViewCell.reuseIdentifier)
        
        describe("SuccessfulQueryDataSource Tests") {
            
            beforeEach {
                viewmodel = MockSearchCellVM()
                datasource = SuccessfulQueryDataSource()
            }
            
            it( "SuccessfulQueryDataSource data load test" ) {
                datasource?.load(value: [viewmodel!], at: 0)
                let sectionCount = datasource?.numberOfSections(in: tableview)
                let rowCount = datasource?.tableView(tableview, numberOfRowsInSection: 0)
                expect( sectionCount ).to( equal( 1 ), description : "Section count should be 1" );
                expect( rowCount ).to( equal( 1 ), description : "Row count should be 1" );
            }
        }
    }
}
