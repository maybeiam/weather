//
//  WeatherResultVMSpec.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

@testable import Weather
import Quick
import Nimble
import Result
import ReactiveSwift

class MockWeatherResultModel : WeatherResultModelInputs, WeatherResultModelOutputs, WeatherResultModelInOuts {
    
    internal var inputs: WeatherResultModelInputs { return self }
    
    internal func reloadDataFromServer() {
        var weather = WEATHER(with: NSDictionary() )
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:MM";
        weather.observation_time = formatter.string(from: Date())
        rac_weather.value = weather
    }
    
    internal var outputs: WeatherResultModelOutputs { return self }
    
    internal var rac_loading: MutableProperty<Bool> = MutableProperty( false )
    internal var rac_weather: MutableProperty<WEATHER?> = MutableProperty( nil )
    
}

class MockWeatherResultModel_Loading : MockWeatherResultModel {
    override internal func reloadDataFromServer() {
        rac_loading.value = true
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 2.0) { 
            self.rac_loading.value = false
        }
    }
}

class WeatherResultVMSpec: QuickSpec {
    
    override func spec() {
        
        var viewmodel : WeatherResultVM?
        var model : MockWeatherResultModel?
        var loadingModel : MockWeatherResultModel_Loading?
        describe("Weather Result View Model Tests") {
            
            beforeEach {
                model = MockWeatherResultModel()
                loadingModel = MockWeatherResultModel_Loading()
            }
            it( "reload data test" ) {
                viewmodel = WeatherResultVM(with: model!)
                
                let newvm = MutableProperty<WeatherResultCellVMInOuts?>( nil )
                newvm <~ viewmodel!.outputs.rac_weather
                viewmodel!.inputs.reloadData()
                
                expect( newvm.value ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "new view model value after reload request should not be nil")
            }
            
            it( "loading flag test" ) {
                viewmodel = WeatherResultVM(with: loadingModel!)
                
                let isloading = MutableProperty<Bool>( false )
                isloading <~ viewmodel!.outputs.rac_isLoading
                viewmodel!.inputs.reloadData()
                expect( isloading.value ).to(equal( true ), description : "loading value after reload signal should be true")
                
                waitUntil(timeout: 4.0, action: { (done) in
                    sleep(3)
                    expect( isloading.value ).to(equal(false), description: "loading value after finishing reload signal should be false " )
                    done()
                })
                
            }
        }
    }
    
}
