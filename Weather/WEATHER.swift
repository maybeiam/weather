//
//  WEATHER.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 27..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit

public struct WEATHER: DataModelType, ListType {
    public var weatherIcon : IMAGE?
    public var observation_time : String?
    public var weatherDesc : String?
    public var humidity : String?
    
    public init(with nsobject: NSObject) {
        weatherIcon = nsobject.value(forKey: "weatherIcon") as? IMAGE
        observation_time = nsobject.value(forKey: "observation_time") as? String
        weatherDesc = nsobject.value(forKey: "weatherDesc") as? String
        humidity = nsobject.value(forKey: "humidity") as? String
    }
    
    public init(with dictionary: NSDictionary) {
        let weatherIconUrl = (dictionary.object(forKey: "weatherIconUrl") as? NSArray)?.object(at: 0) as? NSDictionary
        weatherIcon = IMAGE( with : weatherIconUrl ?? NSDictionary() )
        observation_time = dictionary.object(forKey: "observation_time" ) as? String
        weatherDesc = ((dictionary.object(forKey: "weatherDesc") as? NSArray)?.object(at: 0) as? NSDictionary)?.object(forKey: "value") as? String
        humidity = dictionary.object(forKey: "humidity" ) as? String
    }
    
    public static func array(from dictionary: NSDictionary) -> Array<WEATHER> {
        let data = dictionary.object(forKey: "data") as? NSDictionary
        guard let current_condition = data?.object(forKey: "current_condition") as? NSArray else {
            return Array<WEATHER>()
        }
        
        return current_condition.map{ element in
            let dictionary = element as! NSDictionary
            return WEATHER(with: dictionary)
        }
    }
}
