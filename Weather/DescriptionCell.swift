//
//  DescriptionCell.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

internal final class DescriptionCell: WeatherResultTableViewCell, CellWithReuseIdentifier {
    
    internal static var reuseIdentifier: String = "DescriptionCellReuseIdentifier"
    
    override internal func bindViewModel() {
        guard let viewmodel = viewmodel else {
            return
        }
        descriptionLabel.rac_text <~ viewmodel.outputs.rac_weatherDescription.producer.observe(on: UIScheduler())
    }
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
