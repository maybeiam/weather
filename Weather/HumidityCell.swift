//
//  HumidityCell.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

internal final class HumidityCell: WeatherResultTableViewCell, CellWithReuseIdentifier {
    
    internal static var reuseIdentifier: String = "HumidityCellReuseIdentifier"
    override internal var viewmodel: WeatherResultCellVMInOuts? {
        didSet {
            bindViewModel()
        }
    }
    
    override internal func bindViewModel() {
        guard let viewmodel = viewmodel else {
            return
        }
        humidityLabel.rac_text <~ viewmodel.outputs.rac_humidity.producer.observe(on: UIScheduler())
    }
    
    
    @IBOutlet weak var humidityLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
    }
    
}
