//
//  DataSource.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 28..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

protocol CellWithReuseIdentifier {
    static var reuseIdentifier : String { get }
}

protocol WeatherTableViewDataSource : UITableViewDataSource {
    func configureCell( cell : UITableViewCell, at indexPath : IndexPath )
}
