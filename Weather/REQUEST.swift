//
//  REQUEST.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 28..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit

public struct REQUEST: DataModelType, ListType {
    public var type : String?
    public var query : String?
    
    public init(with nsobject: NSObject) {
        type = nsobject.value(forKey: "type") as? String
        query = nsobject.value(forKey: "query") as? String
    }
    
    public init(with dictionary: NSDictionary) {
        type = dictionary.object(forKey: "type" ) as? String
        query = dictionary.object(forKey: "query" ) as? String
    }
    
    public static func array(from dictionary: NSDictionary) -> Array<REQUEST> {
        let data = dictionary.object(forKey: "data") as? NSDictionary
        guard let request = data?.object(forKey: "request") as? NSArray else {
            return Array<REQUEST>()
        }
        
        return request.map{ element in
            let dictionary = element as! NSDictionary
            return REQUEST(with: dictionary)
        }
    }
}
