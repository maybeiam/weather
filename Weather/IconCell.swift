//
//  IconCell.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

internal final class IconCell: WeatherResultTableViewCell, CellWithReuseIdentifier {
    
    internal static var reuseIdentifier: String = "IconCellReuseIdentifier"
    
    override internal func bindViewModel() {
        guard let viewmodel = viewmodel else {
            return
        }
        iconImageView.rac_image <~ viewmodel.outputs.rac_weatherIcon.producer.observe(on: UIScheduler())
    }
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
