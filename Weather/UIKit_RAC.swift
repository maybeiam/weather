//
//  UIKit_RAC.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 28..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

struct AssociatedKey {
    static var labeltext : UInt8 = 1
    static var image : UInt8 = 2
    static var isHidden : UInt8 = 3
}

func synchronize( key :  AnyObject, closure : () -> Void ) {
    objc_sync_enter(key)
    closure()
    objc_sync_exit(key)
}

extension UIView {
    public var rac_isHidden : MutableProperty<Bool> {
        get {
            if objc_getAssociatedObject(self, &AssociatedKey.isHidden) == nil {
                synchronize(key: self ) { [unowned self] in
                    let property = MutableProperty(self.isHidden)
                    property.producer.startWithValues { newValue in
                        self.isHidden = newValue
                    }
                    objc_setAssociatedObject(self, &AssociatedKey.isHidden, property, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
                }
            }
            return objc_getAssociatedObject(self, &AssociatedKey.isHidden) as! MutableProperty<Bool>
        }
    }
}

extension UILabel {
    public var rac_text : MutableProperty<String?> {
        get {
            if objc_getAssociatedObject(self, &AssociatedKey.labeltext) == nil {
                synchronize(key: self ) { [unowned self] in
                    let property = MutableProperty(self.text )
                    property.producer.startWithValues { newValue in
                        self.text = newValue
                    }
                    objc_setAssociatedObject(self, &AssociatedKey.labeltext, property, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
                }
            }
            return objc_getAssociatedObject(self, &AssociatedKey.labeltext) as! MutableProperty<String?>
        }
    }
}

extension UIImageView {
    public var rac_image : MutableProperty<UIImage?> {
        get {
            if objc_getAssociatedObject(self, &AssociatedKey.image) == nil {
                synchronize(key: self ) { [unowned self] in
                    let property = MutableProperty(self.image)
                    property.producer.startWithValues { newValue in
                        self.image = newValue
                    }
                    objc_setAssociatedObject(self, &AssociatedKey.image, property, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
                }
                
            }
            return objc_getAssociatedObject(self, &AssociatedKey.image) as! MutableProperty<UIImage?>
        }
    }
}
