//
//  SearchModel.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 27..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

protocol SearchModelInputs : class {
    func searchRequestReceived( query : String )
    func fetchSuccessfulQuery()
}

protocol SearchModelOutputs : class {
    var rac_searchResults : MutableProperty<Array<SingleQuery>> { get }
    var noResultSignal : Signal<String, NoError> { get }
    var weatherSearchResultSignal : Signal<Array<WEATHER>, NoError> { get }
    var requestWeatherDataPair : (request : REQUEST, weather : WEATHER) { get }
}

protocol SearchModelInOuts : class {
    var inputs : SearchModelInputs { get }
    var outputs : SearchModelOutputs { get }
}


internal final class SearchModel: SearchModelInputs, SearchModelOutputs, SearchModelInOuts {
    
    unowned internal var inputs: SearchModelInputs { return self }
    
    internal func searchRequestReceived(query: String) {
        if query.characters.count <= 0 {
            return
        }
        network.request(api: APIs.search(query: query)).startWithSignal { (signal, disposable) in
            signal.observeResult({ [unowned self] (result) in
                if result.error == nil {
                    let dictionary = result.value
                    if let msg = dictionary?.object(forKey: "msg") as? String {
                        self.noResultObserver.send(value: msg)
                    } else {
                        self.storage.insertSuccessfulQuery(query: query)
                        self.storage.fetchSuccessfulQuery().startWithSignal { [unowned self] (sig, dis) in
                            
                            sig.observeResult({ [unowned self] (result) in
                                if result.error == nil {
                                    self._searchResults = result.value ?? Array<SingleQuery>()
                                }
                            })
                            self.disposables.append(dis)
                        }
                        let weathers = WEATHER.array(from: dictionary ?? NSDictionary() )
                        if weathers.count > 0 {
                            self._lastWeather = weathers.last!
                        }
                        let request = REQUEST.array(from: dictionary ?? NSDictionary() )
                        if request.count > 0 {
                            self._lastRequest = request.last!
                        }
                        self.weatherSearchResultObserver.send(value: weathers )
                        
                    }
                }
            })
            disposables.append(disposable)
        }
    }
    
    internal func fetchSuccessfulQuery() {
        self.storage.fetchSuccessfulQuery().startWithSignal { (signal, disposable) in
            signal.observeResult({ [unowned self] (result) in
                if result.error == nil {
                    self._searchResults = result.value ?? Array<SingleQuery>()
                }
            })
            disposables.append(disposable)
        }
    }
    
    
    unowned internal var outputs : SearchModelOutputs { return self }
    
    internal var rac_searchResults: MutableProperty<Array<SingleQuery>> {
        guard let _ = _rac_searchResults else {
            _rac_searchResults = MutableProperty( Array<SingleQuery>() )
            return _rac_searchResults!
        }
        return _rac_searchResults!
    }
    
    internal var requestWeatherDataPair: (request: REQUEST, weather: WEATHER) {
        return ( request : _lastRequest ?? REQUEST( with : NSDictionary()), weather : _lastWeather ?? WEATHER(with : NSDictionary()) )
    }
    
    internal var noResultSignal: Signal<String, NoError> {
        guard let _ = _noResultSignal else {
            let pipe = Signal<String,NoError>.pipe()
            _noResultSignal = pipe.0
            _noResultObserver = pipe.1
            
            return _noResultSignal!
        }
        return _noResultSignal!
    }
    
    internal var weatherSearchResultSignal : Signal<Array<WEATHER>, NoError> {
        guard let _ = _weatherSearchResultObserver else {
            let pipe = Signal<Array<WEATHER>,NoError>.pipe()
            _weatherSearchResultSignal = pipe.0
            _weatherSearchResultObserver = pipe.1
            
            return _weatherSearchResultSignal!
        }
        return _weatherSearchResultSignal!
    }
    
    
    fileprivate let storage : StorageType
    fileprivate let network : NetworkType
    
    init( storage : StorageType, network : NetworkType ) {
        self.storage = storage
        self.network = network
    }
    
    init() {
        self.storage = Storage(with: PersistentStorageDataSource.coredata )
        self.network = Network()
    }
    
    fileprivate var _searchResults : Array<SingleQuery> = Array<SingleQuery>() {
        didSet {
            rac_searchResults.value = _searchResults
        }
    }
    fileprivate var _rac_searchResults : MutableProperty<Array<SingleQuery>>?
    
    fileprivate var _noResultObserver : Observer<String, NoError>?
    fileprivate var _noResultSignal : Signal<String, NoError>?
    fileprivate var noResultObserver : Observer<String, NoError> {
        guard let _ = _noResultObserver else {
            let pipe = Signal<String,NoError>.pipe()
            _noResultSignal = pipe.0
            _noResultObserver = pipe.1
            
            return _noResultObserver!
        }
        return _noResultObserver!
        
    }
    
    fileprivate var _weatherSearchResultObserver : Observer<Array<WEATHER>, NoError>?
    fileprivate var _weatherSearchResultSignal : Signal<Array<WEATHER>, NoError>?
    fileprivate var weatherSearchResultObserver : Observer<Array<WEATHER>, NoError> {
        guard let _ = _weatherSearchResultObserver else {
            let pipe = Signal<Array<WEATHER>,NoError>.pipe()
            _weatherSearchResultSignal = pipe.0
            _weatherSearchResultObserver = pipe.1
            
            return _weatherSearchResultObserver!
        }
        return _weatherSearchResultObserver!
    }
    
    fileprivate var _lastRequest : REQUEST?
    fileprivate var _lastWeather : WEATHER?
    
    lazy var disposables = Array<Disposable>()
    
    deinit {
        disposables.forEach { (disposable) in
            disposable.dispose()
        }
        disposables.removeAll()
    }
}
