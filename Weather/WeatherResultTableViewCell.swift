//
//  WeatherResultTableViewCell.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit

internal class WeatherResultTableViewCell: UITableViewCell {
    
    internal var viewmodel : WeatherResultCellVMInOuts? {
        didSet {
            bindViewModel()
        }
    }
    
    internal func bindViewModel() {
        guard let _ = viewmodel else {
            return
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
