//
//  WeatherResultViewController.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class WeatherResultViewController: UIViewController {
    
    let datasource = WeatherResultDataSource()
    var viewmodel : WeatherResultVMInOuts?
    internal static func instantiate( with viewmodel : WeatherResultVMInOuts ) -> WeatherResultViewController {
        let viewcontroller = Storyboard.Weather.instantiate(WeatherResultViewController.self)
        viewcontroller.viewmodel = viewmodel
        
        return viewcontroller
    }
    
    lazy var disposables = Array<Disposable>()

    @IBOutlet weak var mainTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindViewModel()

        mainTableView.dataSource = datasource
        mainTableView.rowHeight = 80.0
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector( reload ), for: .valueChanged )
        mainTableView.refreshControl = refreshControl
        
        guard let viewmodel = viewmodel else {
            return
        }
        
        viewmodel.inputs.initialLoad()
        // Do any additional setup after loading the view.
    }
    
    func reload() {
        guard let viewmodel = viewmodel else {
            return
        }
        viewmodel.inputs.reloadData()
    }
    
    func bindViewModel() {
        guard let viewmodel = viewmodel else {
            return
        }
        let weatherDisposable = viewmodel.outputs.rac_weather
            .signal
            .observe(on: UIScheduler())
            .skipNil()
            .observeValues { [unowned self] (cellvm) in
                self.datasource.load(value: [cellvm], at: 0)
                self.mainTableView.reloadData()
            }!
        disposables.append(weatherDisposable)
        let loadingDisposable = viewmodel.outputs.rac_isLoading
            .signal
            .observe(on: UIScheduler())
            .filter{ loading in return loading == false }
            .observeValues { [unowned self] (loading) in
                self.mainTableView.refreshControl?.endRefreshing()
            }!
        disposables.append(loadingDisposable)
    }
    
    deinit {
        disposables.forEach { (disposable) in
            disposable.dispose()
        }
        disposables.removeAll()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
