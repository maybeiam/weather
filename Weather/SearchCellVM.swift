//
//  SearchCellVM.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 28..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

protocol SearchCellVMOutputs : class {
    var rac_query : MutableProperty<String> { get }
}

protocol SearchCellVMInouts : class {
    var outputs : SearchCellVMOutputs { get }
}

internal final class SearchCellVM : SearchCellVMInouts, SearchCellVMOutputs {
    
    init( with string : String ) {
        _query = string
    }
    
    unowned internal var outputs: SearchCellVMOutputs { return self }
    internal var rac_query: MutableProperty<String> {
        get {
            guard let _ = _rac_query else {
                _rac_query = MutableProperty( _query )
                return _rac_query!
            }
            return _rac_query!
            
        }
        set {
            _rac_query = newValue
        }
    }
    
    fileprivate var _query : String = "" {
        didSet {
            rac_query.value = _query
        }
    }
    fileprivate var _rac_query: MutableProperty<String>?
}

