//
//  WeatherResultCellsVM.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

protocol WeatherResultCellVMOutputs : class {
    var rac_humidity : MutableProperty<String?> { get }
    var rac_weatherIcon : MutableProperty<UIImage?> { get }
    var rac_observationTime : MutableProperty<String?> { get }
    var rac_weatherDescription : MutableProperty<String?> { get }
}

protocol WeatherResultCellVMInOuts : class {
    var outputs : WeatherResultCellVMOutputs { get }
}

internal final class WeatherResultCellsVM : WeatherResultCellVMOutputs,  WeatherResultCellVMInOuts{
    unowned var outputs : WeatherResultCellVMOutputs { return self }
    
    internal var rac_weatherIcon: MutableProperty<UIImage?> = MutableProperty( nil )
    internal var rac_humidity: MutableProperty<String?> = MutableProperty( nil )
    internal var rac_observationTime: MutableProperty<String?> = MutableProperty( nil )
    internal var rac_weatherDescription: MutableProperty<String?> = MutableProperty( nil )
    
    fileprivate var _datamodel : WEATHER
    
    init( with datamodel : WEATHER ) {
        _datamodel = datamodel
        
        rac_humidity.value = _datamodel.humidity
        rac_observationTime.value = _datamodel.observation_time
        rac_weatherDescription.value = _datamodel.weatherDesc
        
        
        if _datamodel.weatherIcon == nil || _datamodel.weatherIcon!.url == nil {
            self.rac_weatherIcon.value = nil
        } else {
            URLSession.shared.dataTask(with: _datamodel.weatherIcon!.url!.https ) { [unowned self] (data, response, error) in
                guard let responseData = data else {
                    return
                }
                guard let image = UIImage(data: responseData) else {
                    return
                }
                self.rac_weatherIcon.value = image
            }.resume()
        }
    }
}
