//
//  SearchVM.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 28..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

protocol SearchVMInputs : class {
    func searchButtonPressed( query : String )
    func didSelectSuggestion( at index : Int )
    func didEnterSearchTextField()
    func didLeaveSearchTextField()
}

protocol SearchVMOutputs : class {
    var rac_suggestionList : MutableProperty<Array<String>> { get }
    var rac_suggestionListCellVM : MutableProperty<Array<SearchCellVMInouts>> { get }
    var rac_SuggestionListTableViewIsHidden : MutableProperty<Bool> { get }
    var searchRequestReceivedNoDataSignal : Signal<String, NoError> { get }
    var searchRequestReceivedDataSignal : Signal<WeatherResultVMInOuts, NoError> { get }
}

protocol SearchVMInOuts : class {
    var inputs : SearchVMInputs { get }
    var outputs : SearchVMOutputs { get }
}

internal final class SearchVM: SearchVMInputs, SearchVMOutputs, SearchVMInOuts {
    
    unowned internal var inputs: SearchVMInputs { return self }
    internal func searchButtonPressed(query: String) {
        model.inputs.searchRequestReceived(query: query)
    }
    
    internal func didSelectSuggestion(at index: Int) {
        model.inputs.searchRequestReceived(query: _suggestionList[index] )
    }
    
    internal func didEnterSearchTextField() {
        model.inputs.fetchSuccessfulQuery()
        _suggestionListTableViewIsHidden = false
    }
    
    internal func didLeaveSearchTextField() {
        _suggestionListTableViewIsHidden = true
    }
    
    unowned internal var outputs: SearchVMOutputs { return self }
    
    internal var rac_suggestionList: MutableProperty<Array<String>> {
        get {
            guard let _ = _rac_suggestionList else {
                let property = MutableProperty( _suggestionList )
                property.producer.startWithValues{ [unowned self] value in
                    self._suggestionList = value
                }
                _rac_suggestionList = property
                return _rac_suggestionList!
            }
            return _rac_suggestionList!
        }
        set {
            _rac_suggestionList = newValue
        }
    }
    
    internal var rac_suggestionListCellVM: MutableProperty<Array<SearchCellVMInouts>> = MutableProperty(Array<SearchCellVM>())
    
    internal var rac_SuggestionListTableViewIsHidden : MutableProperty<Bool> {
        get {
            guard let _ = _rac_SuggestionListTableViewIsHidden else {
                _rac_SuggestionListTableViewIsHidden = MutableProperty( _suggestionListTableViewIsHidden )
                return _rac_SuggestionListTableViewIsHidden!
            }
            return _rac_SuggestionListTableViewIsHidden!
        }
        set {
            _rac_SuggestionListTableViewIsHidden = newValue
        }
    }
    
    internal var searchRequestReceivedNoDataSignal: Signal<String, NoError> {
        guard let _ = _searchRequestReceivedNoDataSignal else {
            let pipe = Signal<String,NoError>.pipe()
            _searchRequestReceivedNoDataSignal = pipe.output
            _searchRequestReceivedNoDataObserver = pipe.input
            
            return _searchRequestReceivedNoDataSignal!
        }
        return _searchRequestReceivedNoDataSignal!
    }
    
    internal var searchRequestReceivedDataSignal : Signal<WeatherResultVMInOuts, NoError> {
        guard let _ = _searchRequestReceivedDataObserver else {
            let pipe = Signal<WeatherResultVMInOuts,NoError>.pipe()
            _searchRequestReceivedDataSignal = pipe.0
            _searchRequestReceivedDataObserver = pipe.1
            
            return _searchRequestReceivedDataSignal!
        }
        return _searchRequestReceivedDataSignal!
    }
    
    fileprivate var _rac_suggestionList : MutableProperty<Array<String>>?
    fileprivate var _rac_SuggestionListTableViewIsHidden : MutableProperty<Bool>?
    
    fileprivate var _suggestionList : Array<String> = Array<String>()
    fileprivate var _suggestionListTableViewIsHidden : Bool = true {
        didSet {
            rac_SuggestionListTableViewIsHidden.value = _suggestionListTableViewIsHidden
        }
    }
    
    fileprivate var _searchRequestReceivedNoDataObserver : Observer<String, NoError>?
    fileprivate var _searchRequestReceivedNoDataSignal : Signal<String, NoError>?
    fileprivate var searchRequestReceivedNoDataObserver : Observer<String, NoError> {
        guard let _ = _searchRequestReceivedNoDataObserver else {
            let pipe = Signal<String,NoError>.pipe()
            _searchRequestReceivedNoDataSignal = pipe.0
            _searchRequestReceivedNoDataObserver = pipe.1
            
            return _searchRequestReceivedNoDataObserver!
        }
        return _searchRequestReceivedNoDataObserver!
        
    }
    
    fileprivate var _searchRequestReceivedDataObserver : Observer<WeatherResultVMInOuts, NoError>?
    fileprivate var _searchRequestReceivedDataSignal : Signal<WeatherResultVMInOuts, NoError>?
    fileprivate var searchRequestReceivedDataObserver : Observer<WeatherResultVMInOuts, NoError> {
        guard let _ = _searchRequestReceivedDataObserver else {
            let pipe = Signal<WeatherResultVMInOuts,NoError>.pipe()
            _searchRequestReceivedDataSignal = pipe.0
            _searchRequestReceivedDataObserver = pipe.1
            
            return _searchRequestReceivedDataObserver!
        }
        return _searchRequestReceivedDataObserver!
        
    }
    
    let model : SearchModelInOuts
    required init( with model : SearchModelInOuts ) {
        self.model = model
        bind()
    }
    
    lazy var disposables = Array<Disposable>()
    
    fileprivate func bind() {
        rac_suggestionList <~ model.outputs.rac_searchResults.map{ list in list.filter{ $0.query != nil }.map{ object -> String in return object.query!  } }
        rac_suggestionListCellVM <~ rac_suggestionList.map{ queries in queries.map{ string in SearchCellVM(with: string) } }
        let noResultDisposable = model.outputs.noResultSignal
            .observeValues{ [unowned self] value in
                self.searchRequestReceivedNoDataObserver.send(value: value)
            }!
        disposables.append(noResultDisposable)
        let weatherSearchDisposable = model.outputs.weatherSearchResultSignal
            .observeValues{ [unowned self] value in
                let weatherResultModel = WeatherResultModel(request: self.model.outputs.requestWeatherDataPair.request, weather: self.model.outputs.requestWeatherDataPair.weather)
                let weatherResultViewModel = WeatherResultVM(with: weatherResultModel )
                self.searchRequestReceivedDataObserver.send(value: weatherResultViewModel )
            }!
        disposables.append(weatherSearchDisposable)
    }
    
    deinit {
        disposables.forEach { (disposable) in
            disposable.dispose()
        }
        disposables.removeAll()
    }
}
