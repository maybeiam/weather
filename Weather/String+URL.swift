//
//  String+URL.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 24..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit

extension String {
    var url : URL? {
        let value = URL(string: self)
        return value ?? nil
    }
}
