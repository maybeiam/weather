//
//  IMAGE.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 27..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit

public struct IMAGE : DataModelType {
    public var image : UIImage?
    public var url : URL?
    
    public init(with nsobject: NSObject) {
        image = nsobject.value(forKey: "image") as? UIImage
        url = nsobject.value(forKey: "url") as? URL
    }
    
    public init(with dictionary: NSDictionary) {
        url = ((dictionary.object(forKey: "value") as? String) ?? "").url
    }
}
