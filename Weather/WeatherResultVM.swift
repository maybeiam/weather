//
//  WeatherResultVM.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

protocol WeatherResultVMInputs : class {
    func reloadData()
    func initialLoad()
}

protocol WeatherResultVMOutputs : class {
    var rac_weather : MutableProperty<WeatherResultCellVMInOuts?> { get }
    var rac_isLoading : MutableProperty<Bool> { get }
}

protocol WeatherResultVMInOuts : class {
    var inputs : WeatherResultVMInputs { get }
    var outputs : WeatherResultVMOutputs { get }
}

internal final class WeatherResultVM : WeatherResultVMInOuts, WeatherResultVMOutputs, WeatherResultVMInputs {
    unowned internal var inputs: WeatherResultVMInputs { return self }
    
    internal func reloadData() {
        model.inputs.reloadDataFromServer()
    }
    
    internal func initialLoad() {
        guard let newValue = model.outputs.rac_weather.value else {
            return
        }
        rac_weather.value = WeatherResultCellsVM(with: newValue)
    }
    
    unowned internal var outputs: WeatherResultVMOutputs { return self }
    
    internal var rac_weather: MutableProperty<WeatherResultCellVMInOuts?> = MutableProperty( nil )
    internal var rac_isLoading: MutableProperty<Bool> = MutableProperty( false )
    
    
    func bind() {
        rac_weather <~ model.outputs.rac_weather.map({ (weather) -> WeatherResultCellVMInOuts? in
            guard let weather = weather else {
                return nil
            }
            return WeatherResultCellsVM(with: weather)
        })
        rac_isLoading <~ model.outputs.rac_loading
    }
    
    let model : WeatherResultModelInOuts
    
    required init( with model : WeatherResultModelInOuts ) {
        self.model = model
        bind()
    }
}
