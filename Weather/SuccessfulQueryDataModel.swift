//
//  SuccessfulQueries.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 25..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit

public struct SingleQuery : DataModelType {
    var query : String?
    public init(with dictionary: NSDictionary) {
        query = dictionary.object(forKey: "query") as? String ?? ""
    }
    
    public init(with nsobject: NSObject) {
        query = nsobject.value(forKey: "query") as? String ?? ""
    }
}
