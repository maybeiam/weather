//
//  DataModel.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 24..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import CoreData

public protocol DataModelType {
    init( with dictionary : NSDictionary )
    init( with nsobject : NSObject )
}

public protocol ListType {
    static func array( from dictionary : NSDictionary ) -> Array<Self>
}
