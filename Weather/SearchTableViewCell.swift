//
//  SearchTableViewCell.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 28..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

internal final class SearchTableViewCell: UITableViewCell, CellWithReuseIdentifier {

    internal static var reuseIdentifier: String = "SearchTableViewCellReuseIdentifier"
    
    weak internal var viewmodel : SearchCellVMInouts? {
        didSet {
            bindViewModel()
        }
    }
    
    internal func bindViewModel() {
        guard let viewmodel = viewmodel else {
            return
        }
        queryLabel.rac_text <~ viewmodel.outputs.rac_query.producer.observe(on: UIScheduler())
    }
    
    @IBOutlet weak var queryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
