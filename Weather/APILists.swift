//
//  APILists.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 24..
//  Copyright © 2017년 weather. All rights reserved.
//
import Foundation

public enum TargetURL {
    private enum Base {
        static let root = "https://api.worldweatheronline.com/free/"
        static let version = "v1"
        static let key = "vzkjnx2j5f88vyn5dhvvqkzc"
    }
    public enum Weather {
        static let string = TargetURL.Base.root + TargetURL.Base.version + "/" + "weather.ashx?fx=yes&format=json" + "&key=" + TargetURL.Base.key
        static let url = TargetURL.Weather.string.url
    }
}

public protocol APISType {
    var request: URLRequest? { get }
}

public enum APIs : APISType {
    case search( query : String )
    
    public var request: URLRequest? {
        switch self {
        case let .search( query ):
            guard let url = (TargetURL.Weather.string + "&q=" + query).url else {
                return nil
            }
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            return request
        }
    }
}
