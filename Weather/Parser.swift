//
//  Parser.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 24..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import Result
import ReactiveSwift

public protocol Parser {
    func parse( with data : Data? ) -> NSDictionary?
}

public protocol ErrorParser {
    func parse( with dictionary : NSDictionary? ) -> NSDictionary?
}

public struct JSONParser : Parser {
    public func parse(with data: Data?) -> NSDictionary? {
        guard let data = data else {
            return nil
        }
        do {
            let dictionary = try JSONSerialization.jsonObject(with: data) as! NSDictionary
            return dictionary
        } catch {
            return nil
        }
    }
}

public struct WeatherErrorParser : ErrorParser {
    public func parse(with dictionary: NSDictionary?) -> NSDictionary? {
        guard let dictionary = dictionary else {
            return ["msg" : "Empty request result error", "code" : "404"] as NSDictionary
        }
        
        guard let data = dictionary.object(forKey: "data") as? NSDictionary else {
            return ["msg" : "Wrong format error", "code" : "404"] as NSDictionary
        }
        
        guard let error = data.object(forKey: "error") as? NSArray else {
            return nil
        }
        
        let msg = (error.object(at: 0) as! NSDictionary).object(forKey: "msg") as! String
        return ["msg" : msg, "code" : "404"] as NSDictionary
    }
}
