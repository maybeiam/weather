//
//  SearchViewController.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 28..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

internal final class SearchViewController: UIViewController {
    
    let datasource = SuccessfulQueryDataSource()
    
    internal static func instantiate() -> SearchViewController {
        let viewcontroller = Storyboard.Main.instantiate(SearchViewController.self)
        
        let model = SearchModel()
        let viewmodel = SearchVM(with: model)
        viewcontroller.viewmodel = viewmodel
        
        return viewcontroller
    }

    var viewmodel : SearchVMInOuts?
    lazy var disposables = Array<Disposable>()
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var mainTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainTableView.dataSource = datasource
        mainTableView.delegate = self
        bindViewModel()
        // Do any additional setup after loading the view.
    }
    
    func bindViewModel() {
        guard let viewmodel = viewmodel else {
            return
        }
        
        viewmodel.outputs.rac_suggestionListCellVM.signal.observe(on: UIScheduler()).observeValues{ [unowned self] list in
            self.datasource.load(value: list, at: 0)
            self.mainTableView.reloadData()
        }
        mainTableView.rac_isHidden <~ viewmodel.outputs.rac_SuggestionListTableViewIsHidden.producer.observe(on: UIScheduler())
        
        let noDataDisposable = viewmodel.outputs.searchRequestReceivedNoDataSignal
            .observe(on: UIScheduler())
            .observeValues{ [unowned self] msg in
                let alertController = UIAlertController(title: "No data message", message: msg, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }!
        disposables.append(noDataDisposable)
        
        let dataReceivedDisposable = viewmodel.outputs.searchRequestReceivedDataSignal
            .observe(on: UIScheduler())
            .observeValues { [unowned self] (weatherVM) in
                let viewController = WeatherResultViewController.instantiate(with: weatherVM)
                self.navigationController?.pushViewController(viewController, animated: true)
            }!
        disposables.append(dataReceivedDisposable)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func searchButtonAction(_ sender: Any) {
        guard let viewmodel = viewmodel else {
            return
        }
        viewmodel.inputs.searchButtonPressed(query: searchTextField.text ?? "" )
    }
    
    deinit {
        disposables.forEach { (disposable) in
            disposable.dispose()
        }
        disposables.removeAll()
    }
}

extension SearchViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewmodel = viewmodel else {
            return
        }
        viewmodel.inputs.didSelectSuggestion(at: indexPath.row)
    }
}

extension SearchViewController : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let viewmodel = viewmodel else {
            return
        }
        viewmodel.inputs.didLeaveSearchTextField()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let viewmodel = viewmodel else {
            return
        }
        viewmodel.inputs.didEnterSearchTextField()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let viewmodel = viewmodel else {
            return true
        }
        textField.resignFirstResponder()
        viewmodel.inputs.searchButtonPressed(query: textField.text ?? "" )
        return true
    }
}
