//
//  Network.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 24..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

public enum NetworkError : Error {
    case unknown
    case wrongurl
    case nodata
}

public protocol NetworkType {
    func request( api : APISType ) -> SignalProducer<NSDictionary, NetworkError>
}

public struct Network : NetworkType {
    
    fileprivate static let parser = JSONParser()
    fileprivate static let errorparser = WeatherErrorParser()
    
    public func request(api: APISType) -> SignalProducer<NSDictionary, NetworkError> {
        
        guard let urlrequest = api.request else {
            return SignalProducer { observer, disposable in
                observer.send(error: .wrongurl )
                observer.sendCompleted()
            }
        }
        
        return SignalProducer { observer, disposable in
            let session = URLSession(configuration: URLSessionConfiguration.default)
            let task = session.dataTask(with: urlrequest) {  data, response, error in
                let dictionary = Network.parser.parse(with: data)
                let dataerror = Network.errorparser.parse(with: dictionary)
                if dataerror == nil {
                    if dictionary == nil {
                        observer.send(error: .unknown)
                        observer.sendCompleted()
                    } else{
                        observer.send(value: dictionary! )
                        observer.sendCompleted()
                    }
                } else {
                    observer.send(value: dataerror!)
                    observer.sendCompleted()
                }
            }
            task.resume()
        }
    }
    
}
