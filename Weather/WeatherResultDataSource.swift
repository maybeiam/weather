//
//  WeatherResultDataSource.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit

enum WeatherResultSection : Int {
    case icon
    case humidity
    case description
    case time
}
internal final class WeatherResultDataSource : NSObject, WeatherTableViewDataSource {
    
    fileprivate var datasource : Dictionary<Int, Array<Any>> = Dictionary<Int, Array<Any>>()
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section  {
        case WeatherResultSection.icon.rawValue :
            let cell = tableView.dequeueReusableCell(withIdentifier: IconCell.reuseIdentifier, for: indexPath)
            configureCell(cell: cell, at: indexPath)
            return cell
        case WeatherResultSection.humidity.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: HumidityCell.reuseIdentifier, for: indexPath)
            configureCell(cell: cell, at: indexPath)
            return cell
        case WeatherResultSection.description.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: DescriptionCell.reuseIdentifier, for: indexPath)
            configureCell(cell: cell, at: indexPath)
            return cell
        case WeatherResultSection.time.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: ObservationTimeCell.reuseIdentifier, for: indexPath)
            configureCell(cell: cell, at: indexPath)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    internal func load( value : Array<Any>, at section : Int ) {
        datasource[section] = value
    }
    
    internal func configureCell(cell: UITableViewCell, at indexPath: IndexPath) {
        let currentCell = cell as! WeatherResultTableViewCell
        guard let currentSection = datasource[0] else {
            return
        }
        unowned let viewmodel = currentSection[0] as! WeatherResultCellVMInOuts
        currentCell.viewmodel = viewmodel
    }
}

