//
//  WeatherResultModel.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 28..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

protocol WeatherResultModelInputs : class {
    func reloadDataFromServer()
}

protocol WeatherResultModelOutputs : class {
    var rac_weather : MutableProperty<WEATHER?> { get }
    var rac_loading : MutableProperty<Bool> { get }
}

protocol WeatherResultModelInOuts : class {
    var inputs : WeatherResultModelInputs { get }
    var outputs : WeatherResultModelOutputs { get }
}

internal final class WeatherResultModel: WeatherResultModelInputs, WeatherResultModelOutputs, WeatherResultModelInOuts {
    
    unowned internal var inputs: WeatherResultModelInputs { return self }
    
    internal func reloadDataFromServer() {
        _loading = true
        guard let request = _request else {
            _loading = false
            return
        }
        guard let query = request.query else {
            _loading = false
            return
        }
        if query.characters.count <= 0 {
            _loading = false
            return
        }
        network.request(api: APIs.search(query: query)).startWithSignal { (signal, disposable) in
            let sig = signal.observeResult({ [unowned self] (result) in
                self._loading = false
                if result.error == nil {
                    let dictionary = result.value
                    let weathers = WEATHER.array(from: dictionary ?? NSDictionary() )
                    if weathers.count > 0 {
                        self._weather = weathers.last!
                    }
                    let request = REQUEST.array(from: dictionary ?? NSDictionary() )
                    if request.count > 0 {
                        self._request = request.last!
                    }
                }
            })!
            disposables.append(sig)
            disposables.append(disposable)
        }
    }
    
    internal var outputs: WeatherResultModelOutputs { return self }
    
    internal var rac_loading: MutableProperty<Bool> {
        get {
            guard let _ = _rac_loading else {
                _rac_loading = MutableProperty( _loading )
                return _rac_loading!
            }
            return _rac_loading!
        }
        set {
            _rac_loading = newValue
        }
    }
    
    internal var rac_weather: MutableProperty<WEATHER?> = MutableProperty( nil )

    fileprivate var _rac_loading : MutableProperty<Bool>?
    fileprivate var _loading : Bool = false {
        didSet {
            rac_loading.value = _loading
        }
    }
    
    fileprivate var _weather : WEATHER? {
        didSet {
            rac_weather.value = _weather
        }
    }
    fileprivate var _request : REQUEST?
    
    fileprivate let network : NetworkType
    
    init( network : NetworkType ) {
        self.network = network
    }
    
    init() {
        self.network = Network()
    }
    
    required init( request : REQUEST, weather : WEATHER ) {
        self.network = Network()
        _request = request
        _weather = weather
        rac_weather = MutableProperty<WEATHER?>( weather )
    }
    
    init( request : REQUEST, weather : WEATHER, network : NetworkType ) {
        self.network = network
        _request = request
        _weather = weather
        rac_weather = MutableProperty<WEATHER?>( weather )
    }
    
    lazy var disposables = Array<Disposable>()
    
    deinit {
        disposables.forEach { (disposable) in
            disposable.dispose()
        }
        disposables.removeAll()
    }
}
