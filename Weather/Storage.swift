//
//  Storage.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 24..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

public protocol StorageRepresentationType {
    var datasource : Fetchable { get }
}

public enum PersistentStorageDataSource : StorageRepresentationType {
    case coredata
    
    public var datasource : Fetchable {
        switch self {
        case .coredata:
            return CoreDataPersistentStorage.shared
        }
    }
}

public enum StorageFetchError : Error {
    case unknown
    case nodata
}

public protocol StorageType : class {
    var persistantStorage : Fetchable { get }
    init( with storage : StorageRepresentationType )
    
    func fetch<T:DataModelType>( query : Query ) -> SignalProducer<Array<T>, StorageFetchError>
    func fetchSuccessfulQuery() -> SignalProducer<Array<SingleQuery>, StorageFetchError>
    func insertSuccessfulQuery( query : String )
}

final public class Storage : StorageType {
    public init(with storage: StorageRepresentationType ) {
        persistantStorage = storage.datasource
    }
    
    public var persistantStorage : Fetchable
    
    public func fetch<T:DataModelType>(query: Query) -> SignalProducer<Array<T>, StorageFetchError> {
        switch query {
        case .search:
            return SignalProducer { [unowned self] observer, disposable in
                self.persistantStorage.fetchSuccesfulQuery().map{ datas in datas.map{ data in T( with : data ) } }.startWithSignal({ (signal, disposable) in
                    signal.observeResult({ (result) in
                        if result.error == nil {
                            observer.send(value: result.value ?? Array<T>())
                        } else {
                            observer.send(error: result.error!)
                        }
                    })
                })
            }
        }
    }
    
    public func fetchSuccessfulQuery() -> SignalProducer<Array<SingleQuery>, StorageFetchError> {
        return fetch(query: .search)
    }
    
    public func insertSuccessfulQuery(query: String) {
        self.persistantStorage.writeSuccesfulQuery(query: query)
    }
}
