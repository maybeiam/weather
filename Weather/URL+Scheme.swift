//
//  URL+Scheme.swift
//  Weather
//
//  Created by Shepherd on 2017. 1. 29..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit

extension URL {
    var https : URL {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        components?.scheme = "https"
        return components?.url ?? self
    }
    
    var http : URL {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        components?.scheme = "http"
        return components?.url ?? self
    }
}
