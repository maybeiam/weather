//
//  PersistentStorage.swift
//  Weather
//
//  Created by 1002220 on 2017. 1. 25..
//  Copyright © 2017년 weather. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result
import CoreData

private let _sharedCoreDataPresistentStorage = CoreDataPersistentStorage( name : "Weather" )
private let _sharedInMemoryCoreDataPresistentStorage = CoreDataPersistentStorage( inmemoryName : "Weather" )

public protocol Fetchable {
    func fetchSuccesfulQuery() -> SignalProducer< Array<NSObject>, StorageFetchError>
    func writeSuccesfulQuery( query : String )
}

final public class CoreDataPersistentStorage {
    fileprivate let storageManagedObjectContext : NSManagedObjectContext
    fileprivate let mainManagedObjectContext : NSManagedObjectContext
    
    public static let shared = _sharedCoreDataPresistentStorage
    public static let sharedInMemory = _sharedInMemoryCoreDataPresistentStorage
    
    init( name : String ) {
        guard let modelUrl = Bundle.main.url(forResource: name, withExtension: "momd") else {
            fatalError("Error loading model from bundle")
        }
        
        guard let mom = NSManagedObjectModel(contentsOf: modelUrl) else {
            fatalError("Error initializing mom from: \(modelUrl)")
        }
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        storageManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        storageManagedObjectContext.persistentStoreCoordinator = psc
        
        mainManagedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        mainManagedObjectContext.parent = storageManagedObjectContext
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docUrl = urls[urls.endIndex-1]
        
        let storeURL = docUrl.appendingPathComponent(name + ".sqlite")
        do {
            try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        } catch {
            fatalError("Error migrating store: \(error)")
        }
    }
    
    init( inmemoryName name : String ) {
        guard let modelUrl = Bundle.main.url(forResource: name, withExtension: "momd") else {
            fatalError("Error loading model from bundle")
        }
        
        guard let mom = NSManagedObjectModel(contentsOf: modelUrl) else {
            fatalError("Error initializing mom from: \(modelUrl)")
        }
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        storageManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        storageManagedObjectContext.persistentStoreCoordinator = psc
        
        mainManagedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        mainManagedObjectContext.parent = storageManagedObjectContext
        
        do {
            try psc.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)
        } catch {
            fatalError("Adding in-memory persistent store failed")
        }
    }
}

extension CoreDataPersistentStorage : Fetchable {
    public func fetchSuccesfulQuery() -> SignalProducer<Array<NSObject>, StorageFetchError> {
        return SignalProducer { [unowned self] observer, disposable in
            let fetched = self.fetch(entity: "SuccessfulQuery", sortBy: "date", ascending: false, fetchLimit: 10)
            observer.send(value: fetched)
            observer.sendCompleted()
        }
    }
    
    public func writeSuccesfulQuery(query: String) {
        let fetched = self.fetch(entity: "SuccessfulQuery", sortBy: "date", ascending: false, fetchLimit: 10)
        let querycount = fetched.filter { (managedObject) -> Bool in
            return managedObject.value(forKey: "query" ) as? String == query as String
        }.count
        if querycount > 0 {
            return
        }
        insert { (moc) in
            if fetched.count > 10 {
                let first = fetched[0]
                first.setValue(query, forKey: "query")
                first.setValue(NSDate(), forKey: "date")
            } else {
                let object = NSEntityDescription.insertNewObject(forEntityName: "SuccessfulQuery", into: moc) as? SuccessfulQuery
                object?.date = NSDate()
                object?.query = query
            }
        }
    }
}

extension CoreDataPersistentStorage {
    internal func insert( context : @escaping (NSManagedObjectContext) -> () ) {
        let privateObjectManagedContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateObjectManagedContext.parent = mainManagedObjectContext
        
        context( privateObjectManagedContext )
        
        do {
            try privateObjectManagedContext.save()
            
            do {
                try self.mainManagedObjectContext.save()
                
                do {
                    try self.storageManagedObjectContext.save()
                } catch {
                    fatalError("Failed to save data to persistent storage")
                }
            } catch {
                fatalError("Failed to save data to persistent storage")
            }
        } catch {
            fatalError("Failed to save data to persistent storage")
        }
    }
    
    fileprivate func fetch( with request : NSFetchRequest<NSManagedObject> ) -> Array<NSManagedObject> {
        do {
            let fetchedObjects = try mainManagedObjectContext.fetch(request)
            return fetchedObjects
        } catch {
            fatalError("Failed to load data from persistent storage")
        }
    }
    
    public func fetch( entity name : String ) -> Array<NSManagedObject> {
        let request = NSFetchRequest<NSManagedObject>(entityName: name)
        return fetch(with: request)
    }
    
    public func fetch( entity name : String, sortBy sortDescriptor : String, ascending : Bool ) -> Array<NSManagedObject> {
        let request = NSFetchRequest<NSManagedObject>(entityName: name)
        request.sortDescriptors = [NSSortDescriptor(key: sortDescriptor, ascending: ascending )]
        return fetch(with: request)
    }
    
    public func fetch( entity name : String, sortBy sortDescriptor : String, ascending : Bool, fetchLimit : Int ) -> Array<NSManagedObject> {
        let request = NSFetchRequest<NSManagedObject>(entityName: name)
        request.sortDescriptors = [NSSortDescriptor(key: sortDescriptor, ascending: ascending )]
        request.fetchLimit = fetchLimit
        return fetch(with: request)
    }
}
